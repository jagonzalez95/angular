export class DestinoViaje {
    nombre: string;
    descripcion: string;
    fechaInicio: Date;
    fechaFin: Date;
    urlImagen: string;

    constructor(n: string, d: string, fi: Date, ff: Date, u: string) {
        this.nombre = n;
        this.descripcion = d;
        this.fechaInicio = fi;
        this.fechaFin = ff;
        this.urlImagen = u;
    }
}

import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  destinos: DestinoViaje[];
  minDate: Date;
  maxDate: Date;

  constructor() {
    this.destinos = [];
    this.minDate = new Date();
  }

  ngOnInit(): void {
    this.minDate = new Date();
  }

  guardar(nombre: string, descripcion: string, fechaIni: Date, fechaFin: Date, url: string) {
    this.destinos.push(new DestinoViaje(nombre, descripcion, fechaIni, fechaFin, url));
    return false;
  }

}
